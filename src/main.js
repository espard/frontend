import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router/index'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requires_login)) {
    if (!localStorage.getItem('access_token')) {
      next({ name: 'LoginPage' })
    } else if(to.name == 'HomePage') {
      next({name: 'ListPage'})
    } else{
      next()
    }
  } else {
    next()
  }
})

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
