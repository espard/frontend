import axios from 'axios'

const getApi = axios.create({
    baseURL: 'http://localhost:8005/api/',
    timeout: 50000
})

getApi.interceptors.request.use(
    (config) => {
        config.headers['X-Requested-With'] = 'XMLHttpRequest';
        config.headers['Accept'] = 'application/json';
        
        let token = localStorage.getItem('access_token');
        
        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
        }
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)


export {getApi}