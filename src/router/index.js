import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginPage from "../views/LoginPage"
import HomePage from "../views/HomePage"
import ListPage from "../views/ListPage"
import FavsListPage from "../views/FavsListPage"
import ProductPage from "../views/ProductPage"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: HomePage,
    meta: {
      requires_login: false
    },
    children: [
      {
        path: '/',
        name: 'ListPage',
        component: ListPage,
      },
      {
        path: '/details/:id',
        name: 'ProductPage',
        component: ProductPage,
        meta: {
          requires_login: true
        },
      },
      {
        path: '/favs',
        name: 'FavsListPage',
        component: FavsListPage,
        meta: {
          requires_login: true
        },
      },
    ]
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: LoginPage
  }
]

const router = new VueRouter({
  mode: "history",
  routes
})

export default router
